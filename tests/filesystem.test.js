/**
 * @jest-environment jsdom
 */

import { logoutHandler, newFileHandler, updateFileHandler, deleteFileHandler, loadFilesHandler } from '../Backend/static/javascript/filesystem.js';

// Mock localStorage and window.location
beforeEach(() => {
  fetch.mockClear();
  Object.defineProperty(window, 'localStorage', {
    value: {
      getItem: jest.fn(() => 'admin'), // Simulating an admin user
      setItem: jest.fn(),
      removeItem: jest.fn()
    },
    writable: true
  });

  // Enhanced mocking of window.location
  delete window.location;
  window.location = {
    href: '',
    assign: jest.fn(), // Mocking navigation method assign
    replace: jest.fn() // Mocking navigation method replace
  };

  document.body.innerHTML = `
  <div class="nav-bar">
    <button id="edit-permissions-button" style="display:none;"></button>
    <button id="logout-button"></button>
  </div>
  <div class="popup" id="new-file-popup">
    <form id="new-file-form">
      <input id="new-file-name" />
      <textarea id="new-file-data"></textarea>
    </form>
  </div>
  <div class="popup" id="update-file-popup">
    <form id="update-file-form">
      <input id="update-file-name" />
      <textarea id="update-data"></textarea>
    </form>
  </div>
  <div class="popup" id="delete-file-popup">
    <form id="delete-file-form">
      <input id="delete-file-name" />
    </form>
  </div>
  <ul id="file-list"></ul>
`;

  global.fetch = jest.fn(() => Promise.resolve({
    json: () => Promise.resolve({ message: 'success' }),
    ok: true
  }));
});

// Test for newFileHandler
test('newFileHandler should prevent default and call backend', async () => {
  const event = { preventDefault: jest.fn() };
  document.getElementById('new-file-name').value = 'TestFile';
  document.getElementById('new-file-data').value = 'Hello world';

  await newFileHandler(event);

  expect(event.preventDefault).toHaveBeenCalled(); // Ensures the default action is prevented
  expect(fetch).toHaveBeenCalled(); // Checks if fetch is called
  const fetchCall = fetch.mock.calls[0];
  expect(fetchCall[0]).toBe('http://127.0.0.1:5000/files'); // Check the URL
  expect(fetchCall[1].method).toBe('POST'); // Check the method
  expect(fetchCall[1].body).toContain('TestFile'); // Checks if body contains 'TestFile'
  expect(fetchCall[1].body).toContain('Hello world'); // Checks if body contains 'Hello world'
});

// Test for logoutHandler
test('logoutHandler should clear localStorage and redirect', () => {
  logoutHandler();

  expect(localStorage.removeItem).toHaveBeenCalledWith('username'); // Checks if localStorage.removeItem was called
  expect(window.location.href).toBe('login.html'); // Checks redirection
});

// Test for updateFileHandler
test('updateFileHandler should prevent default and call backend to update file', async () => {
  const event = { preventDefault: jest.fn() };
  document.getElementById('update-file-name').value = 'UpdatedFile';
  document.getElementById('update-data').value = 'Updated content';

  await updateFileHandler(event);

  expect(event.preventDefault).toHaveBeenCalled();
  expect(fetch).toHaveBeenCalledWith('http://127.0.0.1:5000/updateFile', expect.objectContaining({
    method: 'PUT',
    body: expect.stringContaining('UpdatedFile'),
    body: expect.stringContaining('Updated content')
  }));
});

test('deleteFileHandler should prevent default and call backend to delete file', async () => {
  const event = { preventDefault: jest.fn() };
  document.getElementById('delete-file-name').value = 'ToDeleteFile';

  await deleteFileHandler(event);

  expect(event.preventDefault).toHaveBeenCalled();
  // Update expected URL and add expected headers and body to match the actual fetch call
  expect(fetch).toHaveBeenCalledWith(
    `http://127.0.0.1:5000/files/ToDeleteFile`, // Make sure the URL is exactly as it should be
    {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: "ToDeleteFile",
        userToPass: null  // This should reflect your actual logic, it appears user might not be set or mocked correctly
      })
    }
  );
});



// Test for loadFilesHandler
test('loadFilesHandler should call backend and populate file list', async () => {
  await loadFilesHandler();

  expect(fetch).toHaveBeenCalled();
  expect(fetch.mock.calls[0][0]).toBe('http://127.0.0.1:5000/files'); // Now checking the first call in this test
  // Ensure you mock the fetch response to contain actual file data if checking DOM manipulation
});
