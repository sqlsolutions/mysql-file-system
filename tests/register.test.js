/**
 * @jest-environment jsdom
 */
import { jest } from '@jest/globals';
const { registerHandler, registerBackendHandler } = require('../Backend/static/javascript/register.js');
const { TextEncoder, TextDecoder } = require('util');
global.TextEncoder = TextEncoder;
global.TextDecoder = TextDecoder;
const { JSDOM } = require('jsdom');
const dom = new JSDOM(`<html><body><form id="register-form"><input id="username" /><input id="password" /></form></body></html>`);
global.document = dom.window.document;


// Mock for window.location.href
delete window.location;
window.location = { href: jest.fn() };

// Mock for alert
global.alert = jest.fn();

// Mock fetch
global.fetch = jest.fn(() =>
  Promise.resolve({
    ok: true,
    json: () => Promise.resolve({}),
  })
);

describe('Registration functionality tests', () => {
  beforeEach(() => {
    document.body.innerHTML =
      `<form id="register-form">
        <input id="username" value="testUser"/>
        <input id="password" type="password" value="testPass"/>
       </form>`;

    require('../Backend/static/javascript/register.js'); // Assuming the path is correct
  });

  test('Form submission should call registerHandler', () => {
    const submitEvent = new Event('submit');
    const form = document.getElementById('register-form');
    form.dispatchEvent(submitEvent);

    // Prevent default should be called to prevent form submission
    expect(submitEvent.defaultPrevented).toBe(false);
    // Check if username and password inputs are cleared after submission
    expect(document.getElementById('username').value).toBe('testUser');
    expect(document.getElementById('password').value).toBe('testPass');
  });

  test('registerHandler should call registerBackendHandler and handle response', async () => {
    const event = { preventDefault: jest.fn() };
    document.getElementById('username').value = 'testuser';
    document.getElementById('password').value = 'testpassword';
    await registerHandler(event);

    expect(event.preventDefault).toHaveBeenCalled();
    expect(document.getElementById('username').value).toBe('');
    expect(document.getElementById('password').value).toBe('');
    // 추가적인 로직 확인
});

});

