import { visualizeBackendHandler, transformToTreeData, hasPermission } from '../Backend/static/javascript/visualize';
beforeEach(() => {
  document.body.innerHTML = `
    <button id="logout-button"></button>
    <button id="new-file-button"></button>  // Add this line
    <div id="new-file-popup"></div>  // Add this line if your code interacts with this element
  `;
  localStorage.setItem('username', 'testUser');
  fetch.mockClear();
});

// Mocking the fetch API and other external modules
global.fetch = jest.fn(() => Promise.resolve({
  ok: true,
  json: () => Promise.resolve({ files: [
    { id: 1, parent: -1, name: "Folder 1", is_file: false },
    { id: 2, parent: 1, name: "Sub folder 1", is_file: false },
    { id: 3, parent: 1, name: "Sub folder 2", is_file: false },
    { id: 4, parent: 2, name: "File 1", is_file: true },
    { id: 5, parent: 2, name: "File 2", is_file: true },
    { id: 6, parent: 3, name: "File 3", is_file: true },
    { id: 7, parent: 3, name: "File 4", is_file: true },
    { id: 8, parent: 1, name: "Folder 2", is_file: false }
  ]})
}));

describe('transformToTreeData', () => {
  it('transforms file list to tree structure correctly', () => {
    const files = [
      { id: 1, parent: -1, name: "Folder 1", is_file: false },
      { id: 2, parent: 1, name: "Sub folder 1", is_file: false },
      { id: 3, parent: 1, name: "Sub folder 2", is_file: false }
    ];
    const treeData = transformToTreeData(files);
    expect(treeData.name).toBe("Folder 1");
    expect(treeData.children).toHaveLength(2);
    expect(treeData.children[0].name).toBe("Sub folder 1");
    expect(treeData.children[1].name).toBe("Sub folder 2");
  });
});

describe('hasPermission', () => {
  it('determines permission access correctly', async () => {
    // Mock the backend handlers for users and permissions
    jest.mock('../Backend/static/javascript/filesystem', () => ({
      loadUsersBackendHandler: () => Promise.resolve([{ id: 1, username: 'testUser' }]),
      loadPermissionsBackendHandler: () => Promise.resolve([{ user_id: 1, file_id: 1, r_perm: true }])
    }));
    
    const permissionResult = await hasPermission(1);
    expect(permissionResult).toBe(false);
  });

  it('handles user without permission', async () => {
    jest.mock('../Backend/static/javascript/filesystem', () => ({
      loadUsersBackendHandler: () => Promise.resolve([{ id: 2, username: 'anotherUser' }]),
      loadPermissionsBackendHandler: () => Promise.resolve([{ user_id: 2, file_id: 1, r_perm: false }])
    }));

    const permissionResult = await hasPermission(1);
    expect(permissionResult).toBe(false);
  });
});
