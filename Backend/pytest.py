"""
This file tests the backend app.py with the database and works on the csl machine. 
This file is not used in the CI/CD Pipeline since it requires being run on the csl machine directly to test with the database.
"""

from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_cors import CORS
import unittest
import json
from appClone import app, db
from typing import Dict, Tuple, Any

class FlaskAppTests(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self) -> None:
        """
        Tear down the test by truncating all tables in the test database.
        """
        #self.truncate_all_tables()
        pass

    def truncate_all_tables(self) -> None:
        """
        Truncate all tables in the database.
        """
        with app.app_context():
            # Get all table names from the metadata
            allTableNames = db.metadata.tables.keys()

            # Iterate over each table and truncate it
            for tableName in allTableNames:
                db.session.query(tableName).delete()
                db.session.commit()

    def test_get_files(self) -> None:
        # Example test for GET /files route
        response = self.app.get('/files')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('files', data)

    def test_create_file(self) -> None:
        # Example test for POST /files route
        post_data = {
            'name': 'Test File',
            'data': 'Test content',
            'created': '2024-04-26 06:00:00',
            'updated': '2024-04-26 06:00:00',
            'is_file': True,
            'userToPass': 'Admin'
        }
        response = self.app.post('/files', data=json.dumps(post_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('message', data)
        self.assertEqual(data['message'], 'File created')

    
    def test_create_user(self) -> None:
        post_user_data = {
            'username': 'Test User',
            'password': 'pssword',
        }
        response = self.app.post('/users', data=json.dumps(post_user_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('message', data)
        self.assertEqual(data['message'], 'User created')

    def test_get_users(self) -> None:
        # Example test for GET /files route
        response = self.app.get('/users')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('users', data)

    def test_new_directory(self) -> None:
        """Tests the new directory button backend implementation"""
        post_data = {
            'name': 'Test Directory',
            'data': '',
            'created': '2024-04-26 06:00:00',
            'updated': '2024-04-26 06:00:00',
            'is_file': False,
            'userToPass': 'Admin'
        }
        response = self.app.post('/files', data=json.dumps(post_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('message', data)
        self.assertEqual(data['message'], 'File created')

    def test_rename_file(self) -> None:
        """Tests the rename file button backend implementation"""
        rename_data = {
            'oldName': 'Test File',
            'newName': 'New Test File',
            'updated': '2024-04-26 06:00:00',
            'userToPass': 'Admin'
        }
        response = self.app.put('/renameFile', data=json.dumps(rename_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('message', data)
        self.assertEqual(data['message'], 'File renamed')

    def test_update_file(self) -> None:
        """Tests the update file button backend implementation"""
        update_data = {
            'name': 'New Test File',
            'data': 'New File Data',
            'updated': '2024-04-26 06:00:00',
            'userToPass': 'Admin'
        }
        response = self.app.put('/updateFile', data=json.dumps(update_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('message', data)
        self.assertEqual(data['message'], 'File updated')

    def test_move_file(self) -> None:
        """Tests the move file button backend implementation"""
        move_data = {
            'name': 'New Test File',
            'destination': 'Test Directory',
            'userToPass': 'Admin'
        }
        response = self.app.put('/moveFile', data=json.dumps(move_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertIsInstance(data, dict)
        self.assertIn('message', data)
        self.assertEqual(data['message'], 'File updated')



if __name__ == '__main__':
    unittest.main()
