/**
 * @file Contains functions for handling login.
 */

/**
 * Adds event listener to login button when window loads.
 * @listens window.onload
 * @see loginHandler
 */
window.onload = function onLoadHandler() {
    // calls loginHandler when login form is submitted
    document.getElementById("login-form").addEventListener("submit", loginHandler);
};

/**
 * Handles the submission of the login form.
 * @param {Event} event The event object passed to the function when the form is submitted.
 * @see loginBackendHandler
 */
function loginHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const username = document.getElementById("username").value;
    console.log("received username: ", username);
    const password = document.getElementById("password").value;

    loginBackendHandler(username, password)
        .then((response) => {
            // if login is valid
            if (response) {
                localStorage.setItem("username", username); // not secure, should use some kind of token really
                window.location.href = "filesystem.html";
            } else {
                // clear username and password fields
                document.getElementById("username").value = "";
                document.getElementById("password").value = "";
                // show error message
                alert("Invalid login, try again.");
            }
        })
        .catch((e) => {
            console.error(e);
            alert("A backend error occurred, try again later.");
        });
}

/**
 * Handles the login authentication with the backend.
 * @param {String} username The username to login.
 * @param {String} password The password to login.
 * @returns {Promise} A promise that resolves to true if the login is valid, false otherwise.
 * @see loginHandler
 */
function loginBackendHandler(username, password) {
    // false response is assumed to be due to incorrect username or password, distinction is unnecessary
    return (
        new Promise((resolve, reject) => {
            const jsonBody = JSON.stringify({ username, password });
            console.log(jsonBody);

            fetch("http://127.0.0.1:5000/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: jsonBody
            })
                .then((r) => {
                    console.log(r);
                    if (r.ok) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        })
    );
}

export { loginHandler, loginBackendHandler };