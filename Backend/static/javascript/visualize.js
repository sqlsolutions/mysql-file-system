/**
 * @file Contains functions for visualizing the filesystem.
 */
import { loadPermissionsBackendHandler, loadUsersBackendHandler } from "./filesystem.js";
export let currentDirectory = 1;

/**
 * Handles visualization of files.
 * Called every time a form is submitted to rerender.
 * @see visualizeBackendHandler
 * @see transformToTreeData
 * @see createTreeVisualization
 */
export function visualizeHandler() {
    console.log("visualizing...");
    visualizeBackendHandler().then((files) => {
        // console.log(files);
        const treeData = transformToTreeData(files);
        // console.log(treeData);
        createTreeVisualization(treeData);
    });
}

/**
 * Handles loading files from the backend.
 * @returns {Promise} A promise that resolves with the files from the backend and rejects with an error message.
 * @see visualizeHandler
 */
function visualizeBackendHandler() {
    return (
        new Promise((resolve, reject) => {
            setTimeout(() => {
                fetch("http://127.0.0.1:5000/files", {
                    method: "GET",
                    headers: {
                        "Content-Type": "application/json"
                    },
                })
                    .then((r) => {
                        if (!r.ok) {
                            throw (new Error(r));
                        }
                        return (r.json());
                    })
                    .then((d) => {
                        // console.log("data", d);
                        resolve(d.files); //  resolves with files
                    })
                    .catch((e) => {
                        reject(e);
                    });
            }, 500);
        })
    );
}

/**
 * Transforms the files into a hierarchical data tree.
 * @param files The files fetched from the backend.
 * @returns {node} The root node of the tree.
 * @see visualizeHandler
 */
function transformToTreeData(files) {
    let nodes = {};
    let rootNodes = [];

    // filter out all files that are in the trash bin, with a parent of -2
    files.filter(file => file.parent != -2).forEach(file => {
        // if node doesn't exist yet, create one
        let node = nodes[file.id] = nodes[file.id] || {};
        node.name = file.name;
        node.children = node.children || [];
        node.id = file.id;
        node.is_file = file.is_file;

        if (file.parent != -1) {
            // if the parent doesn't exist yet, create an empty one to avoid pushing to an nonexisting array
            nodes[file.parent] = nodes[file.parent] || {};
            nodes[file.parent].children = nodes[file.parent].children || [];
            nodes[file.parent].children.push(node);
        } else {
            rootNodes.push(node);
        }
    });

    return (rootNodes[0]);
}

/**
 * Creates the actual tree using d3.js.
 * @param treeData The root of the hierarchical data tree.
 * @see visualizeHandler
 * @see hasPermission
 * @see passCurrentDirectory
 * @see fetchFileData
 */
function createTreeVisualization(treeData, users, permissions) {
    const container = d3.select("#tree-container");
    const width = parseInt(container.style("width"));
    const height = parseInt(container.style("height"));
    container.selectAll("*").remove();

    const svg = d3.select("#tree-container")
        .append("svg")
        .attr("viewBox", `0 0 ${width} ${height}`);

    // initialize tree
    const root = d3.hierarchy(treeData);
    const treeLayout = d3.tree().size([height, width * 0.7]);
    const tree = treeLayout(root);

    const g = svg.append("g")
        .attr("transform", "translate(100,0)");

    // draw links
    g.selectAll(".link")
        .data(tree.links())
        .enter()
        .append("path")
        .attr("class", "link")
        .attr("fill", "none")
        .attr("stroke", "black")
        .attr("d", d3.linkHorizontal()
            .x(d => d.y)
            .y(d => d.x));

    // draw nodes
    const node = g.selectAll(".node")
        .data(root.descendants())
        .enter()
        .append("g")
        .attr("class", "node")
        .attr("transform", d => `translate(${d.y},${d.x})`);

    // make a circle for a file and a square for a directory
    // color shape purple if current directory
    // color blue if user has permission to file, black if not
    node.each(function (d) {
        const element = d3.select(this);
        hasPermission(d.data.id).then(hasPerm => {
            const color = d.data.id === currentDirectory ? "purple" :
                hasPerm ? "blue" : "black";

            if (d.data.is_file) {
                element.append("circle")
                    .attr("r", 10)
                    .style("fill", color);
            } else {
                element.append("rect")
                    .attr("width", 20)
                    .attr("height", 20)
                    .attr("x", -10)
                    .attr("y", -10)
                    .style("fill", color);
            }
        }).catch(error => {
            console.error("Error checking permissions:", error);
            if (d.data.is_file) {
                element.append("circle")
                    .attr("r", 10)
                    .style("fill", "black");
            } else {
                element.append("rect")
                    .attr("width", 20)
                    .attr("height", 20)
                    .attr("x", -10)
                    .attr("y", -10)
                    .style("fill", "black");
            }
        });
    });

    // add labels with file/directory names to nodes
    node.append("text")
        .attr("dy", 5)
        .attr("x", d => d.children ? -20 : 20)
        .style("text-anchor", d => d.children ? "end" : "start")
        .attr("paint-order", "stroke")
        .attr("stroke", "white")
        .attr("stroke-width", 5)
        .text(d => d.data.name);

    // clicking a circle sets the currentDirectory to the fileID
    // clicking a square displays file info
    // rerender the tree on click since the current directory changes
    node.on("click", (event, d) => {
        hasPermission(d.data.id).then(hasPerm => {
            // if you don't have permission to open the file/directory
            if (!hasPerm) {
                alert("You do not have the permission to open this.");
                return;
            }
            // if directory, change current directory and rerender tree
            if (!d.data.is_file) {
                d3.select("#tree-container").selectAll("*").remove();
                if (hasPerm) {
                    currentDirectory = d.data.id;
                    passCurrentDirectory();
                    createTreeVisualization(treeData);
                }
            }
            // else if file, fetch the file data and display it
            else {
                // check for valid permissions to read the file
                if (hasPerm) {
                    let fileName = d.data.name;
                    fetchFileData(fileName).then((file) => {
                        let fileName = file.name;
                        let fileData = file.data;
                        // remove " GMT" from end since it's not GMT
                        let fileCreated = file.created.slice(0, -4);
                        let fileUpdated = file.updated.slice(0, -4);

                        const fileContent = d3.select("#file-display");
                        fileContent.selectAll("text").remove();

                        d3.select("#display-name").append("text").text(`${fileName}`);
                        d3.select("#display-data").append("text").text(`${fileData}`);
                        d3.select("#display-created").append("text").text(`${fileCreated}`);
                        d3.select("#display-updated").append("text").text(`${fileUpdated}`);
                    });
                }
            }
        }).catch(e => {
            console.error("Failed to check permissions:", e);
            alert("Error checking permissions, action cannot be completed.");
        });
    });
}

/**
 * Retrieves a single file's data from backend.
 * @param fileName The name of the file to fetch.
 * @returns {Promise} A promise that resolves with the file data and rejects with an error message.
 * @see createTreeVisualization
 */
function fetchFileData(fileName) {
    return (
        new Promise((resolve, reject) => {
            const jsonBody = JSON.stringify({ fileName });
            console.log(jsonBody);

            fetch("http://127.0.0.1:5000/getSingleFile", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: jsonBody
            })
                .then((r) => {
                    if (!r.ok) {
                        throw (new Error(r));
                    }
                    return (r.json());
                })
                .then((d) => {
                    // console.log("data", d);
                    resolve(d); //  resolves with file
                })
                .catch((e) => {
                    reject(e);
                });
        })
    );
}

/**
 * Sends a currentDirectory update to the backend.
 * @see createTreeVisualization
 */
export function passCurrentDirectory() {
    const jsonBody = JSON.stringify({ currentDirectory });
    console.log(jsonBody);

    fetch("http://127.0.0.1:5000/directory", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: jsonBody
    })
        .then((r) => {
            if (!r.ok) {
                throw (new Error(r));
            }
            return (r.json());
        })
        .then((d) => {
            console.log(d);
        })
        .catch((e) => {
            console.error(e);
        });
}

/**
 * Determines whether user has access to the file or not.
 * Very inefficient, fetches all permissions every time it's called.
 * @param id The ID of the file to check permissions for.
 * @returns {Promise} A promise that resolves with the file's read permission and rejects with an error message.
 * @see createTreeVisualization
 * @see loadUsersBackendHandler
 * @see loadPermissionsBackendHandler
 */
function hasPermission(id, users, permissions) {
    return (
        new Promise((resolve, reject) => {
            const usersLoaded = loadUsersBackendHandler();
            const permsLoaded = loadPermissionsBackendHandler();
            const username = localStorage.getItem("username");

            Promise.all([usersLoaded, permsLoaded]).then(([users, permissions]) => {
                let userId = null;
                for (const user in users) {
                    if (users[user].username === username) {
                        userId = users[user].id;
                    }
                }

                if (userId === null) {
                    console.log("userId cannot be found");
                    resolve(false); // resolve with false if the user ID isn't found
                }

                for (const permId in permissions) {
                    const perm = permissions[permId];
                    if (perm.user_id === userId && perm.file_id === id) {
                        resolve(perm.r_perm); // resolve with the file's read permission
                    }
                }
                resolve(false); // resolve with false if no matching permission is found
            }).catch((e) => {
                console.error(e);
                alert("A backend error occurred, try again later.");
                reject(e); // reject the promise in case of error
            });
        })
    );
}

export { visualizeBackendHandler, transformToTreeData, hasPermission };