/**
 * @file Contains functions for handling registration.
 */

/**
 * Adds event listener to registration button when window loads.
 * @listens window.onload
 * @see registerHandler
 */
window.onload = function onLoadHandler() {
    // calls registerHandler when registration form is submitted
    document.getElementById("register-form").addEventListener("submit", registerHandler);
};

/**
 * Handles the submission of the login form.
 * @param {Event} event The event object passed to the function when the form is submitted.
 * @see registerBackendHandler
 */
function registerHandler(event) {
    event.preventDefault(); // prevents page from reloading

    const username = document.getElementById("username").value;
    console.log("received username: ", username);
    const password = document.getElementById("password").value;

    // clear username and password fields
    document.getElementById("username").value = "";
    document.getElementById("password").value = "";

    registerBackendHandler(username, password)
        .then((response) => {
            // if registration is approved
            if (response) {
                alert("Registration successful, you can now login.");
                window.location.href = "login.html";
            } else {
                // show error message
                alert("Invalid registration, try another username.");
            }
        })
        .catch((e) => {
            console.error(e);
            alert("A backend error occurred, try again later.");
        });
}

/**
 * Handles the registration validation with the backend.
 * @param {String} username The username to register.
 * @param {String} password The password to register.
 * @returns {Promise} A promise that resolves to true if the registration is valid, false otherwise.
 * @see registerHandler
 */
function registerBackendHandler(username, password) {
    // false response is assumed to be due to username already existing
    return (
        new Promise((resolve, reject) => {
            const jsonBody = JSON.stringify({ username, password });
            console.log(jsonBody);

            fetch("http://127.0.0.1:5000/registerUser", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: jsonBody
            })
                .then((r) => {
                    console.log(r);
                    if (r.ok) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        })
    );
}

export { registerHandler, registerBackendHandler };